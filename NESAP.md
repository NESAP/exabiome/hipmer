# HipMer

HipMer - High Performance Meraculous assembler for large organisms and metagenomes.

# Baseline benchmark problem

## Description

There are two benchmarks, one for a single genome assembly (Human), 
and the other for a mock metagenome assembly (mock150).

### Datasets

The input datasets can be retrieved from HPSS at NERSC:

Human: (700GB total uncompressed)

https://portal.nersc.gov/archive/home/r/regan/www/HipMer/s_1_1_sequence.fastq.gz
https://portal.nersc.gov/archive/home/r/regan/www/HipMer/s_1_2_sequence.fastq.gz

Mock150: (200GB total uncompressed)

http://portal.nersc.gov/archive/home/r/regan/www/HipMer/hipmer_mock150_data.tar.gz

This data should be uncompressed into a widely stripped parallel filesystem.

### Build and install

HipMer requires Berkeley UPC, UPC++ and underlying compilers that support C11 and C++14
along with MPI libraries and a recent verison of CMake

For these tests we used the most recent version of Berkeley UPC 2019.4.0 and UPC++ 2019.3.0
as pre-compiled modules on edison

```

git clone https://bitbucket.org/berkeleylab/hipmeraculous.git
cd hipmeraculous
git checkout 64ec5fed2e0ed63a12679a9fd3dfd7bce2aab33b

.misc_deploy/build_envs.sh install .edison_deploy/env-upcatomics.sh

```

Inspecting .edison_deploy/env-upcatomics.sh will reveal the entire build and run environment:
```

module load PrgEnv-intel/6.0.4
module load gcc
module load git
module load cmake
module load bupc-narrow/2019.4.0
module load upcxx/2019.3.0
export HIPMER_ENV=edison-upcatomics
export HIPMER_BUILD_TYPE="Release"
export HIPMER_BUILD_OPTS="-DHIPMER_USE_UPC_ATOMIC_API=1"
export CC=$(which cc)
export CXX=$(which CC)
export MPICC=$(which cc)
export MPICXX=$(which CC)
```

After those steps, HipMer will be installed under: $SCRATCH/hipmer-install-edison-upcatomics/

And the pipeline can be executed via one of these command lines:

For the Human benchmark:
```
# create an empty directory, cd to it, and place links to the s_1_*_sequence.fastq files therein
sbatch --time=10:00 --nodes=1024 --wrap="$SCRATCH/hipmer-install-edison-upcatomics/bin/hipmer -o . -k 51 --diploidy low --min-depth 4 --bubble-depth-cutoff 1 --benchmark --auto-restart=false -p s_1_1_sequence.fastq,s_1_2_sequence.fastq"

# or if you have placed the input files under: $SCRATCH/hipmer_human_data 
RUN_OPTS="--benchmark" sbatch --time=10:00 --nodes=1024 --wrap="$SCRATCH/hipmer-install-edison-upcatomics/bin/test_hipmer.sh human-benchmark"

```

And for the Mock150 benchmark:

```
# create an empty directory, cd to it, and place a link to the mock.150.bbqc.fastq file therein
sbatch --time=20:00 --nodes=256 --wrap="$SCRATCH/regan/hipmer-install-edison-upcatomics/bin/hipmer -o . -k 21,33,55,77 --meta --cgraph --merge-reads=1 --localize-reads --interleaved mock.150.bbqc.fastq:i275:s30 --canonicalize_output --benchmark --auto-restart=False"

# or if you have placed the mock 150 input files under: $SCRATCH/hipmer_mock150_data
RUN_OPTS="--benchmark" sbatch --time=20:00 --nodes=256 --wrap="$SCRATCH/hipmer-install-edison-upcatomics/bin/test_hipmer.sh mock150"

```

After execution of a successful run, statistics can be gathered by using the parse_run_log.pl script in the HipMer installation
directory on the run.log(.gz) after a successful run.  The Time to Solution using the 'TotalTime'
output field.

```
cat run.log | $SCRATCH/hipmer-install-edison-upcatomics/bin/parse_run_log.pl
```

# References:


* Evangelos Georganas, Aydın Buluç, Jarrod Chapman, Steven Hofmeyr, Chaitanya Aluru, Rob Egan, Leonid Oliker, Daniel Rokhsar and Katherine Yelick, ["HipMer: An Extreme-Scale De Novo Genome Assembler"](http://www.eecs.berkeley.edu/~egeor/sc15_genome.pdf). 27th ACM/IEEE International Conference on High Performance Computing, Networking, Storage and Analysis (SC 2015), Austin, TX, USA, November 2015.

* Evangelos Georganas, Aydın Buluç, Jarrod Chapman, Leonid Oliker, Daniel Rokhsar and Katherine Yelick, ["merAligner: A Fully Parallel Sequence Aligner"](http://www.eecs.berkeley.edu/~egeor/ipdps_genome.pdf). 29th IEEE International Parallel & Distributed Processing Symposium (IPDPS 2015), Hyderabad, INDIA, May 2015.

* Jarrod A Chapman, Martin Mascher, Aydın Buluç, Kerrie Barry, Evangelos Georganas, Adam Session, Veronika Strnadova, Jerry Jenkins, Sunish Sehgal, Leonid Oliker, Jeremy Schmutz, Katherine A Yelick, Uwe Scholz, Robbie Waugh, Jesse A Poland, Gary J Muehlbauer, Nils Stein and Daniel S Rokhsar ["A whole-genome shotgun approach for assembling and anchoring the hexaploid bread wheat genome"](http://genomebiology.biomedcentral.com/articles/10.1186/s13059-015-0582-8) . Genome Biology 2015, 16:26 .

* Evangelos Georganas, Aydın Buluç, Jarrod Chapman, Leonid Oliker, Daniel Rokhsar and Katherine Yelick, ["Parallel De Bruijn Graph Construction and Traversal for De Novo Genome Assembly"](http://www.eecs.berkeley.edu/~egeor/sc14_genome.pdf). 26th ACM/IEEE International Conference on High Performance Computing, Networking, Storage and Analysis (SC 2014), New Orleans, LA, USA, November 2014.


## Figure of Merit

Time to Solution of the asssembly from input fastq to output fasta

# Measurement on Edison

The measurement on Edison was done be done varying the number of nodes.

Edison has 64 GB per node of DDR (~10GB for the OS image) and 24 processors.

Human (version 2205a47f)

| Nodes | Time To Solution |
|-------|------------------|
| 32    | 2416             |
| 64    | 1177             |
| 128   | 653              |
| 256   | 397              |
| 512   | 263              |
| 1024  | 183              |

Mock150 (version 64ec5fed)

| Nodes | Time To Solution |
|-------|------------------|
| 32    | 1963             |
| 64    | 1262             |
| 128   | 1054             |
| 256   | 800              |


# Measurement on Cori Haswell

Cori Haswell nodes have 128 GB of DDR memory and 32 processors.

# Measurement on Cori KNL

Cori Knights Landing nodes have 96 GB of DDR memory (16 GB quad cache) and 68 processors.

# Epilog

